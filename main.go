package main

import (
	"github.com/bear429858935/log4go"
	"github.com/bear429858935/scaffold/bootstrap"
	"github.com/bear429858935/scaffold/micro"
	"github.com/gin-gonic/gin"
	_ "go.uber.org/automaxprocs"
	"service-test/config"
)

func main() {

	conf := config.LoadConfig()
	// new bootstrap
	app := bootstrap.New(conf.ServiceConfig)

	app.Init(
		bootstrap.BeforeStart(func() {
			log4go.Info("微服务启动前")
		}),
		bootstrap.BeforeStop(func() {
			log4go.Info("微服务关闭前")
		}),
		bootstrap.MicroService(func(s *micro.Service) {
			log4go.Info("微服务处理流程 %v", s.Name())
		}),
		bootstrap.HTTPService(func(r *gin.Engine) {

		}),
	)

	if err := app.Run(); err != nil {
		panic(err)
	}

}
