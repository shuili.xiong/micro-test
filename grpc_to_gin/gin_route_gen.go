package grpc_to_gin

import (
	"context"
	"errors"
	"reflect"
	"unicode"

	"github.com/micro/go-micro/v2/logger"
)

func (server *rServer) registerHttp(rcvr interface{}) error {
	server.mu.Lock()
	defer server.mu.Unlock()
	if server.serviceByHttpMap == nil {
		server.serviceByHttpMap = make(map[string]*service)
	}
	s := new(service)
	s.typ = reflect.TypeOf(rcvr)
	s.rcvr = reflect.ValueOf(rcvr)
	sname := reflect.Indirect(s.rcvr).Type().Name()
	if sname == "" {
		logger.Fatal("rpc: no service name for type", s.typ.String())
	}

	if !isExported(sname) {
		s := "rpc Register: type " + sname + " is not exported"
		if logger.V(logger.ErrorLevel, logger.DefaultLogger) {
			logger.Error(s)
		}
		return errors.New(s)
	}
	// TODO 首字母小写
	tmpVal := []rune(sname)
	if unicode.IsUpper(tmpVal[0]) {
		sname = string(tmpVal[0]+32) + sname[1:]
	}

	if _, present := server.serviceByHttpMap[sname]; present {
		return errors.New("rpc: service already defined: " + sname)
	}

	s.name = sname
	s.method = make(map[string]*methodType)
	var methodName string
	// Install the methods

	for m := 0; m < s.typ.NumMethod(); m++ {
		method := s.typ.Method(m)
		// TODO 首字母小写
		methodName = method.Name
		tmpVal := []rune(methodName)
		if unicode.IsUpper(tmpVal[0]) {
			methodName = string(tmpVal[0]+32) + methodName[1:]
		}

		if mt := prepareHttpEndpoint(method); mt != nil {
			s.method[methodName] = mt
		}
	}

	if len(s.method) == 0 {
		s := "rpc Register: type " + sname + " has no exported methods of suitable type"
		if logger.V(logger.ErrorLevel, logger.DefaultLogger) {
			logger.Error(s)
		}
		return errors.New(s)
	}

	server.serviceByHttpMap[s.name] = s
	return nil
}

// prepareEndpoint() returns a methodType for the provided method or nil
// in case if the method was unsuitable.
func prepareHttpEndpoint(method reflect.Method) *methodType {
	mtype := method.Type
	mname := method.Name
	var contextType reflect.Type

	// Endpoint() must be exported.
	if method.PkgPath != "" {
		return nil
	}

	switch mtype.NumIn() {
	case 2:
		contextType = mtype.In(1)
	default:
		if logger.V(logger.ErrorLevel, logger.DefaultLogger) {
			logger.Error("method", mname, "of", mtype, "has wrong number of ins:", mtype.NumIn())
		}
		return nil
	}

	if mtype.NumOut() != 0 {
		if logger.V(logger.ErrorLevel, logger.DefaultLogger) {
			logger.Error("method", mname, "has wrong number of outs:", mtype.NumOut())
		}
		return nil
	}

	return &methodType{method: method, ContextType: contextType}
}

func (m *methodType) prepareGinContext(ctx context.Context) reflect.Value {
	if contextv := reflect.ValueOf(ctx); contextv.IsValid() {
		return contextv
	}

	return reflect.Zero(m.ContextType)
}
