package grpc_to_gin

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	"net/http"
	"reflect"
	"strings"
)

type routeByRpc struct {
	*rServer
}

func Inject(ctx *gin.Engine, handler interface{}) {

	route := routeByRpc{&rServer{}}
	err := route.register(handler)
	if err != nil {
		panic(err)
	}

	for _, v := range route.serviceMap {
		r := ctx.Group(v.name + "/")
		for met, vv := range v.method {
			if !vv.stream {
				r.POST("/"+met, route.Dispatcher)
			}
		}
	}
}

func InjectHttpRoute(ctx *gin.Engine, handler interface{}) {
	var (
		methods []string
	)
	route := routeByRpc{&rServer{}}
	err := route.registerHttp(handler)
	if err != nil {
		panic(err)
	}

	for _, v := range route.serviceByHttpMap {
		r := ctx.Group(v.name + "/")
		for met, vv := range v.method {
			methods = strings.Split(met, "_")
			method := vv
			switch methods[1] {
			case "ANY":
				r.Any("/"+methods[0], func(ctx *gin.Context) {
					method.method.Func.Call([]reflect.Value{v.rcvr, method.prepareContext(ctx)})
				})
			case "GET":
				r.GET("/"+methods[0], func(ctx *gin.Context) {
					method.method.Func.Call([]reflect.Value{v.rcvr, method.prepareContext(ctx)})
				})
			default:
				r.POST("/"+methods[0], func(ctx *gin.Context) {
					method.method.Func.Call([]reflect.Value{v.rcvr, method.prepareContext(ctx)})
				})
			}
		}
	}
}

func (r *routeByRpc) Dispatcher(ctx *gin.Context) {
	var (
		param                   []string
		serviceName, methodName string
	)

	param = strings.Split(ctx.Request.URL.Path, "/")

	serviceName = param[1]
	methodName = param[2]
	// process the standard request flow
	r.mu.Lock()
	service := r.serviceMap[serviceName]
	r.mu.Unlock()

	if service == nil {
		ctx.JSON(http.StatusOK, fmt.Errorf("unknown service %s", serviceName))
		return
	}

	mtype := service.method[methodName]
	if mtype == nil {
		ctx.JSON(http.StatusOK, fmt.Errorf("unknown service %s.%s", serviceName, methodName))
		return
	}

	res, err := r.processRequest(ctx, service, mtype)
	if err == nil {
		ctx.JSON(http.StatusOK, res)
		return
	}
	ctx.JSON(http.StatusBadRequest, err)
	return
}

func (r *routeByRpc) processRequest(ctx *gin.Context, service *service, mtype *methodType) (result interface{}, err error) {

	var (
		argv, replyv reflect.Value
	)

	// Decode the argument value.
	argIsValue := false // if true, need to indirect before calling.
	if mtype.ArgType.Kind() == reflect.Ptr {
		argv = reflect.New(mtype.ArgType.Elem())
	} else {
		argv = reflect.New(mtype.ArgType)
		argIsValue = true
	}

	if argIsValue {
		argv = argv.Elem()
	}
	// reply value
	replyv = reflect.New(mtype.ReplyType.Elem())

	function := mtype.method.Func
	var returnValues []reflect.Value

	if pb, ok := (argv.Interface()).(proto.Message); ok {
		err = jsonpb.Unmarshal(ctx.Request.Body, pb)
	} else {
		err = json.NewDecoder(ctx.Request.Body).Decode(argv.Interface())
	}

	if err != nil {
		return replyv.Interface(), fmt.Errorf("仅支持 application/json err %v", err.Error())
	}

	returnValues = function.Call([]reflect.Value{service.rcvr,
		mtype.prepareContext(ctx),
		reflect.ValueOf(argv.Interface()),
		reflect.ValueOf(replyv.Interface())})
	if rerr := returnValues[0].Interface(); rerr != nil {
		err = rerr.(error)
	}

	return replyv.Interface(), err
}
