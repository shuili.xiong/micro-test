module service-test

go 1.14

require (
	github.com/bear429858935/infrastructure v0.0.7
	github.com/bear429858935/log4go v0.0.4
	github.com/bear429858935/scaffold v0.1.0
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.3
	github.com/micro/go-micro/v2 v2.9.1
	go.uber.org/automaxprocs v1.3.0
)
