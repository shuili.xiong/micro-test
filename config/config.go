package config

import (
	"github.com/bear429858935/infrastructure/mysql"
	scaffolds "github.com/bear429858935/scaffold"
	"time"
)

type Config struct {
	*scaffolds.ServiceConfig `yaml:"serviceConfig"`
}

const (
	serviceName    = "test"
	serviceVersion = "v1"
	RedisName      = "common"
)

/* service/go/membership/v1

serviceConfig:
 service:
  serverVersion: 1.0.0
  serverName: go.micro.srv.membership
  registerTTL: 30
  registerInterval: 10
 monitor:
  interval: 10
  name: ""
  logFile: log.xml
 web:
  port: 8080
  host: 0.0.0.0
 allowOrigins:
  - ""
 allowMethods:
  - GET
  - POST
 jaeger:
  samplerType: "const"
  samplerParam: "1"
  senderType: "http"
  endpoint: "http://127.0.0.1:8500/"


*/

func LoadConfig() *Config {
	sc := &Config{}

	err := scaffolds.LoadCustomServiceConfig("", serviceName, serviceVersion, sc)

	if err != nil {
		panic(err)
	}

	//loadRedis()

	loadMysql()

	return sc
}

//func loadRedis() {
//	err := redis.LoadConfig("", RedisName)
//
//	if err != nil {
//		panic(err)
//	}
//}

func loadMysql() {

	err := mysql.LoadConfig("", "testdb")
	if err != nil {
		panic(err)
	}
	location, err := time.LoadLocation("Asia/Shanghai")
	//set default db
	mysql.SetDefaultdatabase("testdb")
	mysql.GetMysqlDefaultDB().SetTZLocation(location)
	mysql.GetMysqlDefaultDB().DatabaseTZ = location
}
